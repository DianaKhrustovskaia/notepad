﻿using System;
using System.Collections.Generic;

namespace Notepad
{
    class Program
    {
        static List<Note> allNotes = new List<Note>();

        static void Main(string[] args)
        {
            char s;
            while (true)
            {
                Console.WriteLine("Что вы хотите сделать? \nСделать новую запись - 1 \nРедактировать запись - 2 \nПросмотреть запись - 3 \nУдалить запись - 4 \nПросмотреть все записи - 5 \nЗакрыть программу - 6");

                s = Console.ReadKey().KeyChar;

                switch (s)
                {
                    case '1':
                        Create();
                        break;
                    case '2':
                        Edit(GetNote());
                        break;
                    case '3':
                        View(GetNote());
                        break;
                    case '4':
                        Remove(GetNote());
                        break;
                    case '5':
                        AllView();
                        break;
                    case '6':
                        return;
                }

                Console.Clear();
            }
        }

        public static Note GetNote()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine("Введите ID контакта:");
                int i = int.Parse(Console.ReadLine());
                if (i < 1 || i > allNotes.Count)
                {
                    Console.Clear();
                    Console.WriteLine("Неверный номер!");
                    continue;
                }
                return allNotes[i - 1];
            }
        }

        public static void Create()
        {
            Note note = new Note();
            allNotes.Add(note);
            Edit(note);
        }

        public static void Edit(Note note)
        {
            Console.Clear();
            note.Create();
        }

        public static void View(Note note)
        {
            Console.Clear();
            Console.WriteLine(
                $"Имя: {note.name}\n" +
                $"Фамилия: {note.familyname}\n" +
                $"Отчество: {note.fathername}\n" +
                $"Номер: {note.phoneNumber}\n" +
                $"Страна: {note.country}\n" +
                $"День рождения: {note.dateOfBirth}\n" +
                $"Организация: {note.organisation}\n" +
                $"Должность: {note.position}\n" +
                $"Заметки: {note.otherNotes}\n");
            Console.ReadLine();
        }

        public static void Remove(Note note)
        {
            allNotes.Remove(note);
        }

        public static void AllView()
        {
            Console.Clear();

            for (int i = 0; i < allNotes.Count; i++)
            {
                Console.WriteLine($"ID: {i + 1}\nИмя: {allNotes[i].name}\nФамилия: {allNotes[i].familyname}\nНомер: {allNotes[i].phoneNumber}\n");
            }
            Console.ReadLine();
        }
    }

    class Note
    {
        enum Rule { NoEmpty, NeedNumbers, No }

        public string name = "";
        public string familyname = "";
        public string fathername = "";
        public string phoneNumber = "";
        public string country = "";
        public string dateOfBirth = "";
        public string organisation = "";
        public string position = "";
        public string otherNotes = "";

        public void Create()
        {
            GetValue("Введите вашу фамилию", ref familyname, Rule.NoEmpty);
            GetValue("Введите ваше имя", ref name, Rule.NoEmpty);
            GetValue("Введите вашу отчество", ref fathername, Rule.No);
            GetValue("Введите ваш номер телефона", ref phoneNumber, Rule.NeedNumbers);
            GetValue("Введите вашу страну проживания", ref country, Rule.No);
            GetValue("Введите вашу дату рождения", ref dateOfBirth, Rule.No);
            GetValue("Введите вашу организацию", ref organisation, Rule.No);
            GetValue("Введите вашу должность", ref position, Rule.No);
            GetValue("Введите ваши заметки", ref otherNotes, Rule.No);
        }

        private void GetValue(string text, ref string value, Rule rule)
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(text + " (Enter - пропустить)");
                if (value.Length != 0)
                    Console.WriteLine($"Прошлая запись: {value}");
                string a = Console.ReadLine();

                if (value.Length != 0 && a.Length == 0)
                    break;

                if (rule == Rule.NoEmpty || rule == Rule.NeedNumbers)
                {
                    if (a.Length == 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Значение не может быть пустым!");
                        continue;
                    }
                    if (rule == Rule.NeedNumbers)
                    {
                        if (!long.TryParse(a, out long n))
                        {
                            Console.Clear();
                            Console.WriteLine("Значение может содержать только цифры");
                            continue;
                        }
                    }
                }

                value = a;
                break;
            }
        }
    }
}